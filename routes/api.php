<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('/register', 'AuthController@register');
        Route::post('/login', 'AuthController@login');
        Route::get('/logout', 'AuthController@logout');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/course/register', 'CourseController@register');
        Route::get('/course', 'CourseController@getCourses');
        Route::post('/course', 'CourseController@createCourses');
        Route::get('/course/export', 'CourseController@exportCourses');
    });
});
