<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $registrationRoute = "api/v1/auth/register";
    protected $loginRoute = "api/v1/auth/login";
    protected $validPassword = "abcdef";
    protected $validEmail = "validemail@example.com";

    public function setUp(): void
    {
        parent::setUp();
        factory(App\User::class, 1)->create(["email" => $this->validEmail]);
        $this->validPassword = 'abcdef';
    }

    public function testRegistrationValidUser()
    {
        $response = $this->json("POST", $this->registrationRoute, [
            "name" => "testname",
            "email" => "test@example.com",
            "password" => $this->validPassword,
        ]);

        $this->assertEquals(true, $response->json("success"));
        $this->assertEquals("Registration successful!", $response->json("message"));
        $this->assertEquals("testname", $response->json("user.name"));
        $this->assertEquals("test@example.com", $response->json("user.email"));

        $response->assertStatus(201);
    }
    public function testRegistrationDuplicateUser()
    {
        $response = $this->json("POST", $this->registrationRoute, [
            "name" => "testname",
            "email" => "test@example.com",
            "password" => $this->validPassword,
        ]);

        $response = $this->json("POST", $this->registrationRoute, [
            "name" => "testname",
            "email" => "test@example.com",
            "password" => $this->validPassword,
        ]);

        $this->assertEquals(false, $response->json("success"));
        $this->assertEquals("The email has already been taken.", $response->json("errors.email.0"));
        $response->assertStatus(422);
    }

    public function testRegistrationInvalidDetails()
    {
        $response = $this->json("POST", $this->registrationRoute, [
            "name" => "",
            "email" => "invalidemail.com",
            "password" => "",
        ]);

        $this->assertEquals(false, $response->json("success"));
        $this->assertEquals("The email must be a valid email address.", $response->json("errors.email.0"));
        $this->assertEquals("Please enter your name.", $response->json("errors.name.0"));
        $this->assertEquals("Please enter a password.", $response->json("errors.password.0"));

        $response->assertStatus(422);
    }

    public function testLoginValidUser()
    {
        $response = $this->json("POST", $this->loginRoute, [
            "email" => $this->validEmail,
            "password" => $this->validPassword,
        ]);

        $this->assertEquals(true, $response->json("success"));
        $this->assertEquals("Login successful.", $response->json("message"));
        $this->assertEquals("testname", $response->json("user.name"));
        $this->assertEquals($this->validEmail, $response->json("user.email"));
    }

    public function testLoginInvalidUser()
    {
        $response = $this->json("POST", $this->loginRoute, [
            "email" => "invalidemail@example.com",
            "password" => $this->validPassword,
        ]);

        $this->assertEquals(false, $response->json("success"));
        $this->assertEquals("Invalid email or password.", $response->json("message"));
    }
}
