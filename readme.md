# SeamlessHR-Test

##

#### Setup

-   Install dependencies by running `composer install`
-   Run tests by running `composer run test`

## Docs

### User Registration

#### Request

`POST /api/v1/auth/register`

#### Request Body

```
{
    "email": "validemail@example.com",
    "password": "valid password",
    "name": "validname"
}
```

#### Response

```
{
  "success": true,
  "message": "Registration successful!",
  "user": {
    "id": 1,
    "email": "validemail@example.com",
    "name": "validname"
  },
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvcmVnaXN0ZXIiLCJpYXQiOjE1ODM2MjM0NDQsImV4cCI6MTU4MzYyNzA0NCwibmJmIjoxNTgzNjIzNDQ0LCJqdGkiOiJVcW93a293WHNoSVFNZENHIiwic3ViIjo1LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.gQv1iiB_fx4hT4TuFggFUEs5BDn6Yc9SNSwAJMHjQgw"
}
```

### User Login

#### Request

`POST /api/v1/auth/login`

#### Request Body

```
{
    "email": "validemail@example.com",
    "password": "valid password",
}
```

#### Response

```
{
  "success": true,
  "message": "Login successful.",
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1ODM2MjM1NzYsImV4cCI6MTU4MzYyNzE3NiwibmJmIjoxNTgzNjIzNTc2LCJqdGkiOiI4SXlKTllCUWFXSFdMTDF1Iiwic3ViIjo1LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.YRd_xTWjURpquxwEZ1EOfbczO4bA6RFXv6NeJt6KtEY",
  "user": {
    "id": 1,
    "email": "validemail@example.com",
    "name": "validname"
  }
}
```

### Create courses

#### Request

`POST /api/v1/course`

##### Request headers

`Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1ODM3NDExMjksImV4cCI6MTU4Mzc0NDcyOSwibmJmIjoxNTgzNzQxMTI5LCJqdGkiOiJsS2pJMThsb0VJam9XbUlaIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.IuiKYaKFBeqFk3NjsN8WbKMlx8pvp-VmrXSyB-Bq4us`

#### Response

```
{
  "success": true,
  "message": "Courses registered successfully."
}
```

### Course registration

#### Request

`POST /api/v1/course/register`

##### Request headers

`Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1ODM3NDExMjksImV4cCI6MTU4Mzc0NDcyOSwibmJmIjoxNTgzNzQxMTI5LCJqdGkiOiJsS2pJMThsb0VJam9XbUlaIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.IuiKYaKFBeqFk3NjsN8WbKMlx8pvp-VmrXSyB-Bq4us`

#### Request Body

```
{
	"ids": [251,252,253]
}
```

#### Response

```
{
  "success": true,
  "message": "Course registration successful."
}
```

### Get all courses

#### Request

`GET /api/v1/course`

##### Request headers

`Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1ODM3NDExMjksImV4cCI6MTU4Mzc0NDcyOSwibmJmIjoxNTgzNzQxMTI5LCJqdGkiOiJsS2pJMThsb0VJam9XbUlaIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.IuiKYaKFBeqFk3NjsN8WbKMlx8pvp-VmrXSyB-Bq4us`

#### Response

```
{
  "courses": [
    {
      "id": 251,
      "text": "Dolorem quaerat ipsam voluptas corporis vitae delectus architecto.",
      "created_at": "2020-03-08 12:21:44",
      "updated_at": "2020-03-08 12:21:44",
      "title": "Ratione dolores sequi consectetur pariatur exercitationem maiores ut.",
      "unit": 2,
      "facilitator": "Rosamond Morar",
      "users": [
        {
          "id": 1,
          "name": "knfgs",
          "email": "sdfmdsffr@sf.vom",
          "email_verified_at": null,
          "created_at": "2020-03-08 11:59:34",
          "updated_at": "2020-03-08 11:59:34",
          "users": {
            "course_id": 251,
            "user_id": 1,
            "created_at": "2020-03-09 00:36:04",
            "updated_at": "2020-03-09 00:36:04"
          }
        }
      ]
    },
    {
      "id": 252,
      "text": "Totam occaecati possimus perferendis.",
      "created_at": "2020-03-08 12:21:45",
      "updated_at": "2020-03-08 12:21:45",
      "title": "Placeat nemo repudiandae dignissimos ipsa.",
      "unit": 7,
      "facilitator": "Dr. Brad Moen Sr.",
      "users": [
        {
          "id": 1,
          "name": "knfgs",
          "email": "sdfmdsffr@sf.vom",
          "email_verified_at": null,
          "created_at": "2020-03-08 11:59:34",
          "updated_at": "2020-03-08 11:59:34",
          "users": {
            "course_id": 252,
            "user_id": 1,
            "created_at": "2020-03-09 00:36:05",
            "updated_at": "2020-03-09 00:36:05"
          }
        }
      ]
    }
}
```

### Export all courses

#### Request

`GET /api/v1/course/export?format={format}`
`format`: `csv` or `xlsx`

##### Request headers

`Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1ODM3NDExMjksImV4cCI6MTU4Mzc0NDcyOSwibmJmIjoxNTgzNzQxMTI5LCJqdGkiOiJsS2pJMThsb0VJam9XbUlaIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.IuiKYaKFBeqFk3NjsN8WbKMlx8pvp-VmrXSyB-Bq4us`



