<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'title', 'unit', 'facilitator',
    ];

    /**
     * The users who registered for a course
     *
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'course_registration', 'course_id', 'user_id')
        ->as('users')
        ->withTimestamps();
    }
}
