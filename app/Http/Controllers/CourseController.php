<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exports\CoursesExport;
use App\Http\Requests\ExportRequest;
use App\Http\Requests\RegisterCoursesRequest;
use App\Jobs\CreateCourses;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    /**
     * Create 50 random courses
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function createCourses()
    {
        $createCourses = new CreateCourses();
        dispatch($createCourses);
        return response()->json([
          "success" => true,
          "message" => "Courses created successfully!"
        ], 201);
    }

    /**
     * Register user for one or more classes
     *
     * @param App\Http\Requests\RegisterCoursesRequest $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function register(RegisterCoursesRequest $request)
    {
        $validated = $request->validated();
        try {
            $request->user()->courses()->syncWithoutDetaching($validated['ids']);
            return response()->json([
                "success" => true,
                "message" => "Course registration successful.",
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => "Could not register courses.",
            ], 500);
        }
    }

    /**
     * Get all courses
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function getCourses(Request $request)
    {
        $courses = Course::with('users')->get();
        $courses->each(function ($course) {
            $user = $course->users->firstWhere('id', Auth::id());
            if ($user) {
                $course["registration_date"] = $user->users->created_at;
            }
            unset($course->users);
        });

        return response()->json([
          "courses" => $courses
        ]);
    }

    /**
     * Export all courses
     * 
     *  @param Illuminate\Http\Request $request
     */
    public function exportCourses(ExportRequest $request) {
      $validated = $request->validated();
      return Excel::download(new CoursesExport, 'courses.'.$validated["format"]);
    }
}
