<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegistrationRequest;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Register a user
     *
     * @param App\Http\Requests\UserRegistrationRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function register(UserRegistrationRequest $request)
    {
        $validated = $request->validated();
        try {
            $user = User::create([
                'email' => $validated['email'],
                'password' => Hash::make($validated['password']),
                'name' => $validated['name'],
            ]);

            return response()->json([
                "success" => true,
                "message" => "Registration successful!",
                "user" => $user->only('id', 'email', 'name'),
                "token" => auth()->login($user),
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => "Could not register user.",
            ], 500);
        }
    }

    /**
     * Log a user in
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $credentials = $request->only(['email', 'password']);
            if (!$token = auth()->attempt($credentials)) {
                return response()->json([
                    "success" => false,
                    "message" => "Invalid email or password.",
                ], 401);
            }

            return response()->json([
                "success" => true,
                "message" => "Login successful.",
                "token" => $token,
                "user" => auth()->user()->only('id', 'name', 'email'),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => "Could not log user in.",
            ], 500);
        }
    }

    /**
     * Log user out
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json([
            "success" => true,
            "message" => "Logout successful.",
        ], 200);
    }
}
